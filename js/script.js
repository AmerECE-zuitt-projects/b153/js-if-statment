let passed = "You passed the semester. Congratulations";
let failed = "Sorry you failed this semester :(";
let passGrade = 0;

function gradingSystem (grade) {
    if (grade >= 70) {
        console.log("Your grade is", grade+ "%",  passed);
    } else {
        console.log("Your grade is", grade+ "%",  failed);
    };
};

gradingSystem(70);